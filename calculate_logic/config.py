import os

from dotenv import load_dotenv, find_dotenv


load_dotenv(find_dotenv())


DATABASE = {
    'drivername': 'postgresql',
    'host': os.getenv('POSTGRES_HOST'),
    'port': os.getenv('POSTGRES_PORT'),
    'username': os.getenv('POSTGRES_USER'),
    'password': os.getenv('POSTGRES_PASS'),
    'database': os.getenv('POSTGRES_DB'),
}

JUMPS_COEFFICIENT = 0.9
ITERATION_NUMBER = 100
PAGE_RANK_TOP = 25
