import networkx as nx
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.express import bar
from sqlalchemy import create_engine
from sqlalchemy import sql
from sqlalchemy.engine import URL

from calculate_logic.config import DATABASE, JUMPS_COEFFICIENT, ITERATION_NUMBER, PAGE_RANK_TOP


def main():
    engine = create_engine(URL.create(**DATABASE))
    conn = engine.connect()

    links = [obj[0] for obj in conn.execute(
        sql.text('SELECT DISTINCT link FROM items WHERE link <> referrer;')
    ).fetchall()]
    amount = len(links)
    matrix = np.zeros((amount, amount))
    vector = np.full((amount,), 1.0)

    # --------------------------- Calculate PageRank ---------------------------

    for y, y_link in enumerate(links):
        connected_links = [
            obj[0] for obj in conn.execute(
                sql.text(f'SELECT DISTINCT link FROM items WHERE referrer=:referrer;'),
                referrer=y_link
            ).fetchall()
        ]
        for connected_link in connected_links:
            try:
                matrix[y][links.index(connected_link)] = 1
            except ValueError:
                pass

        number = sum(matrix[y])
        if number:
            for x, x_link in enumerate(links):
                matrix[y][x] /= number

    for _ in range(ITERATION_NUMBER):
        vector = vector.dot(matrix)
        number = sum(vector)
        if number:
            for x, x_val in enumerate(vector):
                vector[x] /= number
        vector = np.dot(
            np.column_stack((vector, [1 / amount] * amount)),
            np.matrix([[JUMPS_COEFFICIENT], [1 - JUMPS_COEFFICIENT]])
        )
        vector = np.array(list(map(lambda o: float(o[0]), vector)))

    # --------------------------- Generate matrix visualization ---------------------------

    # df = pd.DataFrame(matrix, index=links, columns=links)
    # html = df.to_html()
    # with open('matrix.html', 'w+') as f:
    #     f.write(html)

    # --------------------------- Generate incoming/outcoming visualization ---------------------------

    G = nx.DiGraph()

    G.add_nodes_from(links)

    edge_lst = []
    for link in links:
        connected_links = [
            obj[0] for obj in conn.execute(
                sql.text(f'SELECT DISTINCT link FROM items WHERE referrer=:referrer;'),
                referrer=link
            ).fetchall()
        ]
        for connected_link in connected_links:
            try:
                coords = (links.index(link), links.index(connected_link))
                if coords[0] != coords[1] and coords not in edge_lst:
                    edge_lst.append(coords)
            except ValueError:
                pass

    G.add_edges_from(edge_lst)

    pos = nx.spring_layout(G)
    for node in G.nodes():
        G.nodes[node]['pos'] = pos[node]

    edge_x = []
    edge_y = []
    for edge in G.edges():
        x0, y0 = G.nodes[edge[0]]['pos']
        x1, y1 = G.nodes[edge[1]]['pos']
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)

    edge_trace = go.Scatter(
        x=edge_x,
        y=edge_y,
        line=dict(width=0.5, color='#888'),
        hoverinfo='none',
        mode='lines'
    )

    node_x = []
    node_y = []
    for node in G.nodes():
        x, y = G.nodes[node]['pos']
        node_x.append(x)
        node_y.append(y)

    node_trace = go.Scatter(
        x=node_x,
        y=node_y,
        mode='markers',
        hoverinfo='text',
        marker=dict(
            showscale=True,
            colorscale='bluered',
            color=[],
            size=10,
            colorbar=dict(
                thickness=15,
                title='Connections number',
                xanchor='left',
                titleside='right'
            ),
            line_width=2
        )
    )

    node_adjacencies = []
    node_text = []
    for node, adjacencies in enumerate(G.adjacency()):
        node_adjacencies.append(len(adjacencies[1]))
        if isinstance(adjacencies[0], int):
            text = links[adjacencies[0]]
        else:
            text = adjacencies[0]
        node_text.append(f'{len(adjacencies[1])}: {text}')

    node_trace.marker.color = node_adjacencies
    node_trace.text = node_text

    fig = go.Figure(
        data=[edge_trace, node_trace],
        layout=go.Layout(
            title='Connections model',
            showlegend=False,
            hovermode='closest',
            margin=dict(b=20, l=5, r=5, t=40),
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)
        )
    )
    fig.show()

    # --------------------------- Generate PageRank visualization ---------------------------

    data = []
    for index, link in enumerate(links):
        data.append({'Link': f'<a href="{link}">{link}</a>', 'PageRank': vector[index]})
    data = sorted(data, key=lambda o: o['PageRank'], reverse=True)[:PAGE_RANK_TOP]

    x_axes = {}
    y_axes = {'categoryorder': 'total ascending'}

    fig = bar(
        data_frame=data,
        x='PageRank',
        y='Link',
        title=f'PageRank TOP-{PAGE_RANK_TOP}',
        orientation='h'
    ).update_xaxes(**x_axes).update_yaxes(**y_axes)
    fig.show()


if __name__ == '__main__':
    main()
