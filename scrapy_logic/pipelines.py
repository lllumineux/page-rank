# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


from sqlalchemy.orm import sessionmaker

from scrapy_logic.models import db_connect, create_items_table, Items


class PageRankPipeline:
    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates items table.
        """
        engine = db_connect()
        create_items_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        """
        Process the item and store to database.
        """
        session = self.Session()
        instance = session.query(Items).filter_by(**item).one_or_none()
        if instance:
            return instance
        page_rank_item = Items(**item)

        try:
            session.add(page_rank_item)
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

        return item
