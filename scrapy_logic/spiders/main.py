import re

import scrapy
from scrapy import Request
from scrapy.utils import spider

from scrapy_logic.settings import DEPTH_LIMIT


class MainSpider(scrapy.Spider):
    name = 'main'
    start_urls = ['https://datalaboratory.one/']

    def parse(self, response, depth_lvl=1, **kwargs):
        if depth_lvl <= DEPTH_LIMIT:
            href_selector = '/html//body//a/@href'
            for href in response.xpath(href_selector).extract():
                full_href = href if re.fullmatch(r'https?://.+\..+', href) else response.urljoin(href)
                full_href = full_href.split('#')[0] if '#' in full_href else full_href
                if re.fullmatch(r'https?://.+\..+', full_href):
                    spider.logger.info(f'Depth level: {depth_lvl}, Link: {full_href}')
                    yield {'link': full_href, 'referrer': response.request.url}
                    yield Request(full_href, callback=self.parse, cb_kwargs={'depth_lvl': depth_lvl + 1})
